"""
This script will read DL_POLY output from ForceFit. It will determine the distance of the scanned bond
and the total energy from the OUTPUT file for each geometry run by ForceFit.

@author: m.kelley
"""

from __future__ import print_function

"""
1. Input Parameters
"""
geom_num = 104 # Number of geometries run by ForceFit
atoms_moved = [7, 28] # Index numbers of scanned atoms

"""
2. Functions
"""
# Simple function for calculating distance between two specified sets of coordinates
def dist(x1, y1, z1, x2, y2, z2):
    xdis = (float(x1) - float(x2)) ** 2
    ydis = (float(y1) - float(y2)) ** 2
    zdis = (float(z1) - float(z2)) ** 2
    sumdis = xdis + ydis + zdis
    dis = sumdis ** 0.5
    return dis

	
"""
3. Reads and stores data
"""
distance_list = [] # a list for storing distances of scanned atoms
energy_list = [] # a list for storing total MD energy

for i in range(1,geom_num+1):
	config_file = open('set_1_geometry_%s/CONFIG' % i)
	l = 0 # keeps track of line number
	for line in config_file:
		data = line.strip().split() #splits CONFIG file into fields separated by a space
		l += 1
		if l == (atoms_moved[0] * 2) + 2:
			atm1x = data[0]
			atm1y = data[1]
			atm1z = data[2]
		if l == (atoms_moved[1] * 2) + 2:
			atm2x = data[0]
			atm2y = data[1]
			atm2z = data[2]
	
	distance = dist(atm1x, atm1y, atm1z, atm2x, atm2y, atm2z)
	distance_list.append(distance)
	config_file.close()
	
	output_file = open('set_1_geometry_%s/OUTPUT' % i)
	l = 0 # keeps track of line number
	m = -6 # variable for determine line number of output
	for line in output_file:
		data = line.strip().split() #splits OUTPUT file into fields separated by a space
		l += 1
		try:
			if data[0] == "step" and data[1] == "eng_tot" and data[2] == "temp_tot":
				m = l # finds line number where output starts
			if l == (m + 5):
				eng_tot = data[3]
		except IndexError:
			pass
	
	energy_list.append(eng_tot)
	output_file.close()
	
"""
4. Prints output data
"""
print("Distance Energy")
for i in range(geom_num):
	print("%s   %s" % (distance_list[i], energy_list[i]))
